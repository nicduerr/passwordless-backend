# Passwordless Login POC

*This is a proof of concept only meant for demonstration purposes and absolutely not ready for production!*


The intention of this project is to get a feel for how the user experience of a passwordless login system for websites and web-applications is and to get an inital understanding of where the technical challenges of implementing such a system lie.

The idea is to have a MFA ([Multi Factor Authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication)) to achieve a high level of security while at the same time *improving* the user experience of the login process.

This system can be used entirely in paralell / as an alternative to an existing login/signup process.

### Technical Overview

```sequence
Browser->LoginServer: LoginRequest
LoginServer->Browser: Token issued
Browser->Mobile Device: Present token as QR code
Note right of Mobile Device: - scan QR code\n- approve sharing \nof credentials\n(username, email, ...)
Mobile Device->LoginServer: confirm login
Note right of LoginServer: associate username\nto token session
Note right of LoginServer: verify user\n credentials (TBD)
LoginServer->Browser: user authenticated
```

1. A login request is issued to the server via a websocket connection which responds with a unique token now associated with this connection.  

2. The browser encodes this token and the address where it can be "redeemed" in a QR code.  
3. The user unlocks his mobile device and scans the code
  * the native app opens, the user checks which credentials the login process requested and approves the sharing of them to complete the login process
  * *the native app version is not implemented for this POC, a simple callback URL and hardcoded dummy credentials are used*

4. The login server can verify the user credentials against a local database or via a public key and decide on the authentication & authorization status of the user

5. The login server notifies the browser via the websocket connection of the result of the authentication/authorization process (and possibly issues a JWT token for auth/authz of further requests - TBD).