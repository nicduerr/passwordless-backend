package main

import (
	"crypto/rand"
	"encoding/base64"
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/gorilla/websocket"
)

const (
	apiKey = "CLIENT-API-KEY-TOP-S3CR3T"
)

type tokenRegistry struct {
	sync.RWMutex
	m map[string]*websocket.Conn
}

type loginRequest struct {
	APIKey string `json:"apikey"`
}

func main() {
	tr := tokenRegistry{m: make(map[string]*websocket.Conn)}

	http.HandleFunc("/login-request", loginRequestHandler(&tr))
	http.HandleFunc("/confirm-login", confirmLoginHandler(&tr))

	var port string
	if port = os.Getenv("PORT"); port == "" {
		port = "8080"
	}
	log.Printf("starting passwordless login server on port %s.\n", port)
	http.ListenAndServe(":"+port, nil)
}

func loginRequestHandler(tr *tokenRegistry) http.HandlerFunc {
	upgrader := websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	return func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println("couldn't upgrade to websocket connection")
			log.Printf("%s\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("500 - Couldn't upgrade to websocket connection."))
			return
		}

		var l loginRequest
		conn.ReadJSON(&l)
		if l.APIKey != apiKey {
			conn.WriteMessage(websocket.TextMessage, []byte("401 - Unauthorized. Invalid API Key."))
			conn.Close()
			return
		}

		token, err := generateRandomString(32)
		if err != nil {
			_ = conn.WriteMessage(websocket.TextMessage, []byte("500 - Internal Server Error."))
			conn.Close()
			log.Println("Error during generation of crypto/rand token. There is something seriously wrong.")
			log.Fatalf("%s\n", err)
		}

		tr.Lock()
		tr.m[token] = conn
		tr.Unlock()
		m := struct {
			Type  string `json:"type"`
			Token string `json:"token"`
		}{
			"TOKEN_ISSUED",
			token,
		}
		if err := conn.WriteJSON(m); err != nil {
			log.Printf("error sending \"TOKEN_ISSUED\" message: %s\n", err)
			return
		}
		log.Printf("login request recieved from %s - issued token: %s\n", conn.RemoteAddr(), token)
	}
}

func confirmLoginHandler(tr *tokenRegistry) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if token == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("400 - Bad Request. No Token Provided."))
			return
		}
		user := r.URL.Query().Get("user")
		if user == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("400 - Bad Request. No User Provided."))
			return
		}

		tr.RLock()
		conn, known := tr.m[token]
		tr.RUnlock()
		if !known {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("401 - Unauthorized. Invalid Token."))
			return
		}
		defer conn.Close()

		m := struct {
			Type       string `json:"type"`
			Authorized bool   `json:"authorized"`
			User       string `json:"user"`
		}{
			"LOGIN_CONFIRMED",
			true,
			user,
		}

		if err := conn.WriteJSON(m); err != nil {
			log.Printf("error sending \"LOGIN_CONFIRMED\" message: %s\n", err)
			return
		}
		log.Printf("confirmation recieved from %s for token: %s - user: %s\n", r.RemoteAddr, token, user)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Login Successful."))

		tr.Lock()
		delete(tr.m, token)
		tr.Unlock()
	}
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func generateRandomString(s int) (string, error) {
	b, err := generateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}
