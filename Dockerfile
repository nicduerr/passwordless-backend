FROM golang:alpine as builder
RUN mkdir /go/src/build 
ADD . /go/src/build/
WORKDIR /go/src/build 
RUN apk add --no-cache git
RUN go get .
RUN go build -o main .

FROM alpine
RUN apk --no-cache --update add ca-certificates
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=builder /go/src/build/main /app/
WORKDIR /app
CMD ["./main"]